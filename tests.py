import unittest
from getblogs import SeleniumFetcher


class TestGetblogs(unittest.TestCase):
	def test_all(self):
		sf = SeleniumFetcher()

		test_cases = {
			# 'http://www.lefashionimage.blogspot.com': {},
			# 'http://atlantic-pacific.blogspot.com': {},
			'http://fifi-lapin.blogspot.com': {'instagram': [u'instagram.com/fifi.lapin'], 'pinterest': [u'pinterest.com/fifi_lapin/']},
			'http://www.pennypincherfashion.com/': {'instagram': [u'instagram.com/PPFgirl'], 'pinterest': [u'pinterest.com/ppfgirl/']},
			'http://aprilandmaystudio.blogspot.com/': {'instagram': [u'instagram.com/aprilandmay']},
			'http://www.neginmirsalehi.com/': {'pinterest': [u'pinterest.com/neginmirsalehi/']},
			# 'http://thefashionfruit.com/': {},
			# 'http://www.soparameninas.net/': {},		# hangs up with "transfering data from google.com..."
			# 'http://witanddelight.com/': {},
			# 'http://www.loveforlacquer.com/': {},
			# 'http://www.oliviapalermo.com/': {},		# hangs up with "transfering data from facebook.com..."
			'http://www.thebeautybybel.com/': {'instagram': [u'instagram.com/thefashionbybel']},
			'http://sprinkleofglitter.blogspot.com/': {'pinterest': [u'pinterest.com/louiseglitter/']},
			# 'http://www.jimchapman.co.uk/': {},
			'http://www.tuulavintage.com/': {'instagram': [u'instagram.com/tuulavintage']},
			'http://theivorylane.com': {'instagram': [u'instagram.com/emilyijackson'], 'pinterest': [u'pinterest.com/emilyjacks/']}
		}

		def deterministic_repr(d):
			items = d.items()
			items.sort(lambda a, b: cmp(a[0], b[0]))
			return repr(items)

		for key, value in test_cases.items():
			self.assertEquals(deterministic_repr(sf.get_other_accounts(key)), deterministic_repr(value))
		
		sf.close()


if __name__ == '__main__':
	unittest.main()