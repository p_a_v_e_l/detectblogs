import re
from urlparse import urlparse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile


DOMAIN_ONLY = 'DOMAIN_ONLY'
DOMAIN_AND_PATH = 'DOMAIN_AND_PATH'

class SeleniumFetcher(object):
	services_configuration = [ 		# order matters
		('blogspot', (r'(www\.)?([^\.]+?)\.blogspot\.(com|co\.uk)', DOMAIN_ONLY)),		# not just these two domains of course
		('pinterest', (r'(www\.)?pinterest\.com', DOMAIN_AND_PATH)),
		# ('facebook', (r'(www\.)?facebook\.com', DOMAIN_AND_PATH)),
		# ('twitter', (r'(www\.)?twitter\.com', DOMAIN_AND_PATH)),
		('instagram', (r'.*?instagram\.com', DOMAIN_AND_PATH)),
		(None, (r'^.+?$', DOMAIN_ONLY))		# websites on their own domains
	]

	def __init__(self):
		profile = FirefoxProfile()
		profile.set_preference('permissions.default.image', 2)		# disabling images
		# make sure that firefox version is compatible with selenium version; for example use firefox 28 with selenium 2.42.1
		self.driver = webdriver.Firefox(profile)
		# self.driver.implicitly_wait(10)

	def fetch_links(self, url):
		self.driver.get(url)
		link_elements = self.driver.find_elements_by_tag_name('a')
		hrefs = [link.get_attribute('href') for link in link_elements if not link.get_attribute('href') is None]
		return [(urlparse(link).netloc, urlparse(link).path) for link in hrefs]		# eg [('example.blogspot.com', '/path/to/entry'), ...]

	def detect_social_network(self, url):
		domain = urlparse(url).netloc
		for key, conf in self.services_configuration:
			regexp, _ = conf
			if re.match(regexp, domain):
				return key

	def filter_social_networks(self, original_url, links):
		original_social_network = self.detect_social_network(original_url)
		for domain, path in links:
			if domain.startswith('www.'):		# chopping www. off for uniformity
				domain = domain[4:]
			if not re.match(r'^/?[^/]*?/?$', path):		# throwing anything but direct links to profiles
				continue		# wouldn't work for some platforms, e.g. bloglovin
			for key, conf in self.services_configuration:
				regexp, flag = conf
				if re.match(regexp, domain):
					if key == original_social_network:
						break
					if flag == DOMAIN_ONLY:
						if key == 'blogspot':		# ugly corner case:
							yield (key, domain.replace('blogspot.co.uk', 'blogspot.com'))
						else:
							yield (key, domain)
					elif flag == DOMAIN_AND_PATH:
						yield (key, domain + path)

	def get_other_account_options(self, original_url):
		links = self.fetch_links(original_url)
		options = {}
		for network, url in self.filter_social_networks(original_url, links):
			if options.get(network) is None:
				options[network] = []
			if not url in options[network]:
				options[network].append(url)
		return options

	def get_other_accounts(self, original_url):		# original_url must contain schema
		original_domain = urlparse(original_url).netloc
		if original_domain.startswith('www.'):		# chopping www. off for uniformity
			original_domain = original_domain[4:]
		options = self.get_other_account_options(original_url)
		original_social_network = self.detect_social_network(original_url)
		approved = {}
		for key, links_seq in options.items():
			for link in links_seq:
				other_options = self.get_other_account_options('http://' + link)
				if original_domain in other_options.get(original_social_network, []):
					if approved.get(key) is None:
						approved[key] = []
					approved[key].append(link)
		return approved

	def close(self):
		self.driver.close()


if __name__ == '__main__':
	sf = SeleniumFetcher()
	print sf.get_other_accounts('http://fifi-lapin.blogspot.com')
	sf.close()